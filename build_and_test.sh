#!/bin/bash

GRN="\u001b[32m"
RST="\u001b[0m"
CYN="\u001b[36m"

VER=v3
VERSION="3.1.1"

count_blocks() {
    cut -d, -f1 $1 | cut -d, -f2 $1| sort -u | wc -l
}


SQLITE3_NOINS="https://gitlab.com/fuzzing/fuzzbench/-/jobs/571865188/artifacts/raw/benchmarks/sqlite3/noins/ossfuzz"
SQLITE3_AFL="https://gitlab.com/fuzzing/fuzzbench/-/jobs/571865219/artifacts/raw/benchmarks/sqlite3/bin/ossfuzz"
SQLITE3_CORPUS='https://www.googleapis.com/download/storage/v1/b/fuzzbench-data/o/2020-05-11%2Fexperiment-folders%2Fsqlite3_ossfuzz-afl%2Ftrial-129001%2Fcorpus%2Fcorpus-archive-0097.tar.gz?generation=1589335061074778&alt=media'

XML_CORPUS='https://www.googleapis.com/download/storage/v1/b/fuzzbench-data/o/2020-05-24%2Fexperiment-folders%2Flibxml2-v2.9.2-afl%2Ftrial-143807%2Fcorpus%2Fcorpus-archive-0097.tar.gz?generation=1590465349932046&alt=media'

PHP_CORPUS='https://www.googleapis.com/download/storage/v1/b/fuzzbench-data/o/2020-05-24%2Fexperiment-folders%2Fphp_php-fuzz-parser-honggfuzz%2Ftrial-141980%2Fcorpus%2Fcorpus-archive-0090.tar.gz?generation=1590459763567914&alt=media'



mkdir -p ${VER}
if [[ -d qemu-${VERSION} ]]; then
    # make -C qemu-${VERSION} clean
    make -C qemu-${VERSION} -j || exit 1
    cp qemu-${VERSION}/x86_64-linux-user/qemu-x86_64 afl-fast-cov_64
    cp qemu-${VERSION}/x86_64-linux-user/qemu-x86_64 ${VER}/afl-fast-cov_64
fi


if [[ ! -e ${VER}/afl-fast-cov_64 ]]; then
	echo "[-] ERROR: missing afl-fast-cov binary! "
	exit 1
fi

for d in ${VER}; do
    rm -rf workdir/out_${d}/*
    python cov.py -q ${d} -i workdir/in -o workdir/out_${d} -m64 -t 5 ./workdir/test
    printf $CYN
    wc -l workdir/out_${d}/*
    printf "$GRN[*] validated edge count = $(cat workdir/out_${d}/* | sort -u | wc -l) \n"
    printf $RST
done

test_pthread() {
    rm -rf workdir/out_pthread
    python cov.py -q ${VER} -i workdir/in -o workdir/out_pthread -m64 -t 5 --trace-per-input ./workdir/thread_test
    printf $CYN
    wc -l workdir/out_pthread/*
}


repeat_sqlite() {
    local edge_log="qemu_${VERSION}_$(date +%s)_edges.log"
    tar -xf sqlite_corpus.tgz
    python cov.py -q ${VER} -i corpus/queue -o corpus/covlogs -m64 -t 5 $1 ./ossfuzz @@
    cat corpus/covlogs/* | cut -d, -f1-2 | sort -u > ${edge_log}
    printf "$GRN[*]$RST unique blocks = $(count_blocks ${edge_log} ) \n"
    printf "$GRN[*]$RST unique edges  = $(wc -l ${edge_log}) \n"
    wc -l corpus/covlogs/* | tail

    local edge_log="qemu_${VERSION}_$(date +%s)_edges.log"
    export AFL_QEMU_TRACE="$(pwd)/${VER}/afl-fast-cov_64"
    export TRACE_OUT_DIR="$(pwd)/corpus/covlogs2"
    mkdir -p $TRACE_OUT_DIR
    time /opt/aflpp/afl-showmap -Q -C -i corpus/queue -o corpus/covlogs2 -t 5000 -- ./ossfuzz @@
    cat corpus/covlogs2/* | cut -d, -f1-2 | sort -u > ${edge_log}
    printf "$GRN[*]$RST unique blocks = $(count_blocks ${edge_log} ) \n"
    printf "$GRN[*]$RST unique edges  = $(wc -l ${edge_log}) \n"
    rm -rf corpus
}

test_sqlite() {
    printf "$GRN[*]$RST testing with sqlite corpus \n"
    rm -rf corpus cov
    wget -qO sqlite_corpus.tgz "$SQLITE3_CORPUS"
    wget -q "$SQLITE3_NOINS"
    tar -xf sqlite_corpus.tgz
    chmod +x ossfuzz
    wget -qO sqlite3-coverage.tgz 'https://www.googleapis.com/download/storage/v1/b/fuzzbench-data/o/2020-05-11%2Fcoverage-binaries%2Fcoverage-build-sqlite3_ossfuzz.tar.gz?generation=1589242154111696&alt=media'
    mkdir cov
    tar -C cov -xf sqlite3-coverage.tgz
    printf "$GRN[*]$RST Collecting coverage with libfuzzer(SanitizerCoverage) \n"
    time ./cov/ossfuzz -dump_coverage=1 -timeout=5 -rss_limit_mb=2048 corpus/queue/* 2>&1 | tail -n1

    repeat_sqlite
    if [[ "$1" = "repeat" ]]; then
        repeat_sqlite --trace-per-input
    fi
    rm -rf cov
    rm -f ossfuzz
    rm -f ossfuzz.*.sancov
}

[[ $# -gt 0 ]] && $1 $2
