#!/usr/bin/env python
import argparse
import glob
import os
import signal
import struct
import time

QEMU_PATH = os.path.dirname(os.path.realpath(__file__))

INPUT_FILE = "/dev/shm/coverage_input" #each input is stored here

parser = argparse.ArgumentParser(description='Fast Forkserver Base Binary Coverage')
 
parser.add_argument('-q', metavar='qemu_folder',  help='afl-fast-cov directory', default=QEMU_PATH)
parser.add_argument('-i', metavar='input_folder',  help='Where to look for input files', required=True)
parser.add_argument('-o', metavar='output_folder', help='Where to store the trace files', required=True)
parser.add_argument('-m', metavar='wordize',choices=["32","64"], help='Architecture to use for QEMU', required=True)
parser.add_argument('-t', nargs='?', metavar="timeout", const=1.0, default=1.0, type=float, help='Timeout in seconds')
parser.add_argument('--trace-per-input', help='Generate coverage trace for each input', default=False, action='store_true')
parser.add_argument('--show-output', help='Show program output (keep stdout/err open)', default=False, action='store_true')
parser.add_argument('--verbose', help='Verbose logging during coverage collection', default=False, action='store_true')
parser.add_argument('cmd', metavar="...", nargs=argparse.REMAINDER, help = 'Command to run, @@ is replaced by input file')


def replace_input(f, file):
    if f == "@@":
        return file
    else:
        return f

args = parser.parse_args()
ARGS = [replace_input(f,INPUT_FILE) for f in args.cmd]
INDIR = args.i
OUTDIR = args.o
QEMU_PATH = "{}/afl-fast-cov_{}".format(args.q, args.m)
TIMEOUT = args.t
print("Running %s %s with timeout %f on %d inputs\n"%(QEMU_PATH, " ".join(ARGS), TIMEOUT, len(glob.glob(INDIR+"/*"))))

child_environment = {"TRACE_OUT_DIR": OUTDIR}
if args.trace_per_input:
    child_environment['AFL_LOG_PER_THREAD'] = "1"

def handle_timeout(pid):
    os.kill(pid, signal.SIGKILL)

class Forkserver:
    def __init__(self, show_output=False):
        self.ctl_out, self.ctl_in = os.pipe()
        self.st_out, self.st_in = os.pipe()
        self.in_file = open(INPUT_FILE, "w+")
        self.show_output = show_output

        fork_pid = os.fork()
        if fork_pid == 0:
            self.child()
        else:
            self.parent()

    def child(self):
        FORKSRV_FD = 198 # from AFL config.h
        os.dup2(self.ctl_out, FORKSRV_FD)
        os.dup2(self.st_in, FORKSRV_FD+1)

        if not self.show_output:
            null_fd = os.open('/dev/null', os.O_RDWR)
            os.dup2(null_fd, 1)
            os.dup2(null_fd, 2)
            os.close(null_fd)

        os.dup2(self.in_file.fileno(),0)
        os.close(self.in_file.fileno())

        os.close(self.ctl_in);
        os.close(self.ctl_out);
        os.close(self.st_in);
        os.close(self.st_out);
        env = child_environment
#       child_environment["AFL_QEMU_DEBUG_MAPS"] = "1"
#       child_environment["AFL_DEBUG"] = "1"
#       os.execve(QEMU_PATH,["afl-fast-cov", "-d", "trace:translate_block"]+ARGS, env)
        os.execve(QEMU_PATH,["afl-fast-cov"]+ARGS, env)
        print("child failed")

    def parent(self):
       os.close(self.ctl_out);
       os.close(self.st_in);
       os.read(self.st_out, 4);

    def run(self, testcase_path):
        self.in_file.truncate(0)
        self.in_file.seek(0)
        with open(testcase_path, 'r') as tc:
            self.in_file.write(tc.read())
        self.in_file.seek(0)

        os.write(self.ctl_in, b"\0\0\0\0")

        pid = struct.unpack("I",os.read(self.st_out, 4))[0]

        signal.signal(signal.SIGALRM, lambda signum,sigfr : handle_timeout(pid))
        signal.setitimer(signal.ITIMER_REAL, TIMEOUT, 0)
        while True:
            try:
                status = os.read(self.st_out, 4)
            except OSError:
                continue
            break
        signal.setitimer(signal.ITIMER_REAL, 0, 0) # disable timer for timeout
        if len(status) == 4:
            status = struct.unpack("I",status)[0]
        return status

if not os.path.isdir(OUTDIR):
    os.mkdir(OUTDIR)
frk = Forkserver(args.show_output)

start = time.time()
count = 0
for tc_path in glob.glob(INDIR+"/*"):
    tc_id = os.path.basename(tc_path)[:9]
    if args.verbose:
        print("run %s" % tc_path)
    rval = frk.run(tc_path)
    count += 1
    if not args.trace_per_input:
        continue
    for trace in glob.glob(OUTDIR+"/trace_*.qemu"):
        if args.verbose:
            print("mv", trace, OUTDIR+"/input_%s_thread_%s"%(tc_id, trace.split("_")[-1]))
        if args.trace_per_input:
            os.rename(trace, OUTDIR+"/input_%s_thread_%s"%(tc_id, trace.split("_")[-1]))

print("[*] Finished executing %d inputs in %d seconds" % (count, time.time() - start))
