/*
   american fuzzy lop++ - high-performance binary-only instrumentation
   -------------------------------------------------------------------

   Originally written by Andrew Griffiths <agriffiths@google.com> and
                         Michal Zalewski

   TCG instrumentation and block chaining support by Andrea Biondo
                                      <andrea.biondo965@gmail.com>

   QEMU 2.12.1 port by Andrea Fioraldi <andreafioraldi@gmail.com>

   Copyright 2015, 2016, 2017 Google Inc. All rights reserved.
   Copyright 2019-2020 AFLplusplus Project. All rights reserved.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at:

     http://www.apache.org/licenses/LICENSE-2.0

   This code is a shim patched into the separately-distributed source
   code of QEMU 2.12.1. It leverages the built-in QEMU tracing functionality
   to implement AFL-style instrumentation and to take care of the remaining
   parts of the AFL fork server logic.

   The resulting QEMU binary is essentially a standalone instrumentation
   tool; for an example of how to leverage it for other purposes, you can
   have a look at afl-showmap.c.

 */

#include "afl-fast-cov.h"
#include "rq_trace.h"
#include "tcg-op.h"


void HELPER(afl_maybe_log)(void *tb_ptr) {

  const TranslationBlock *tb = (TranslationBlock *)tb_ptr;
  const target_ulong cur_loc = tb->pc;

  /* add this edge */
  trace_bb(cur_loc, tb->size, afl_prev_loc);

}


/* Generates TCG code for AFL's tracing instrumentation. */
static void afl_gen_trace(TranslationBlock *tb) {

  if (rq_trace_log_per_run) {
    TCGv_ptr tb_ptr = tcg_const_ptr(tb);
    gen_helper_afl_maybe_log(tb_ptr);
    tcg_temp_free_ptr(tb_ptr);
  }

  TCGv_ptr afl_prev_loc_ptr = tcg_const_ptr(&afl_prev_loc);
  TCGv     cur_loc = tcg_const_tl(tb->pc);
  tcg_gen_st_tl(cur_loc, afl_prev_loc_ptr, 0);

}


void rq_trace_free(void) {
  redqueen_trace_free();
}

void rq_trace_init(void) {
    redqueen_trace_new();
}

bool rq_trace_bb(abi_ulong cur_loc, uint16_t size, abi_ulong prev_loc) {
    return trace_bb(cur_loc, size, prev_loc);
}
