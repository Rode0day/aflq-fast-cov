#include "khash.h"
#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>
#include "afl-fast-cov.h"

#define gettid() syscall(SYS_gettid)

typedef unsigned __int128 uint128_t;
typedef uint128_t khint128_t;

#define INIT_NUM_OF_STORED_TRANSITIONS 0xfffff

/*! @function
  @abstract     64-bit integer hash function
  @param  key   The integer [khint64_t]
  @return       The hash value [khint_t]
 */
#define kh_int128_hash_func(key) (khint32_t)((key)>>33^(key)^(key)<<11) ^ (((key>>64))>>33^((key>>64))^((key>>64))<<11)
/*! @function
  @abstract     64-bit integer comparison function
 */
#define kh_int128_hash_equal(a, b) ((a) == (b))
/*! @function
  @abstract     Instantiate a hash map containing 64-bit integer keys
  @param  name  Name of the hash table [symbol]
  @param  khval_t  Type of values [type]
 */
#define KHASH_MAP_INIT_INT128(name, khval_t)								\
	KHASH_INIT(name, khint128_t, khval_t, 1, kh_int128_hash_func, kh_int128_hash_equal)

KHASH_MAP_INIT_INT128(RQ_TRACE, uint64_t)

#define INIT_TRACE_IP 0xFFFFFFFFFFFFFFFFULL

typedef struct redqueen_trace_s{
    khash_t(RQ_TRACE) *lookup;
    int fd;
    uint64_t last_bb;
} redqueen_trace_t;

static __thread redqueen_trace_t *trace_info = NULL;

static inline redqueen_trace_t* redqueen_trace_new(void){
    redqueen_trace_t* self = (redqueen_trace_t*)malloc(sizeof(redqueen_trace_t));
    pid_t pid = getpid();
    pid_t tid = gettid();
    char* path = NULL;
    assert(getenv("TRACE_OUT_DIR"));
    assert(asprintf(&path, "%s/trace_%05d_%05d.qemu", getenv("TRACE_OUT_DIR"),pid, tid)!=-1);
    self->fd = open(path, O_WRONLY | O_APPEND | O_CREAT, 0644);
    assert(self->fd > 0);
    if (debug_mode) fprintf(stderr, "open trace file %s\n", path);
    free(path);
    self->lookup = kh_init(RQ_TRACE);
    self->last_bb = INIT_TRACE_IP;
    trace_info = self;
    return self;
}


static inline void redqueen_trace_free(void){
    if (trace_info) {
        kh_destroy(RQ_TRACE, trace_info->lookup);
        close(trace_info->fd);
        free(trace_info);
        trace_info = NULL;
    }
}


static inline bool trace_bb(abi_ulong cur_loc, uint16_t size, abi_ulong prev_loc){
    if(!trace_info) redqueen_trace_new();

    khiter_t k;
    int ret = 0;
    redqueen_trace_t* self = trace_info;

    uint64_t to = cur_loc;
    uint64_t from = prev_loc;
    if (from > afl_end_code || from < afl_start_code) from = INIT_TRACE_IP;
    if (to   > afl_end_code || to   < afl_start_code) to   = INIT_TRACE_IP;

    if ((to == INIT_TRACE_IP) && (from == INIT_TRACE_IP))
        return ret;

    uint128_t key = (((uint128_t)from)<<64) | ((uint128_t)to);
    k = kh_get(RQ_TRACE, self->lookup, key); 
//  fprintf(stderr, "%lx,%lx,%d (known %d) (size %u)\n", from, to, size, k != kh_end(self->lookup), kh_size(self->lookup));
    if(k != kh_end(self->lookup)){
//      kh_value(self->lookup, k) += 1; 
    } else{
        k = kh_put(RQ_TRACE, self->lookup, key, &ret); 
        kh_value(self->lookup, k) = 1;
        if (!afl_fork_child || rq_trace_log_per_run) dprintf(self->fd, "%lx,%lx,%d\n", from, to, size);
//      fprintf(stderr, "\t%p,%p,%d (known %d) (size %u)\n", from, to, size, k != kh_end(self->lookup), kh_size(self->lookup));
    }
//  self->last_bb = to;
    return ret;
}
