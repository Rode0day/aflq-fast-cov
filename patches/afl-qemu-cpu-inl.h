/*
   american fuzzy lop - high-performance binary-only instrumentation
   -----------------------------------------------------------------

   Written by Andrew Griffiths <agriffiths@google.com> and
              Michal Zalewski <lcamtuf@google.com>

   Idea & design very much by Andrew Griffiths.

   Copyright 2015, 2016, 2017 Google Inc. All rights reserved.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at:

     http://www.apache.org/licenses/LICENSE-2.0

   This code is a shim patched into the separately-distributed source
   code of QEMU 3.1.1. It leverages the built-in QEMU tracing functionality
   to implement AFL-style instrumentation and to take care of the remaining
   parts of the AFL fork server logic.

   The resulting QEMU binary is essentially a standalone instrumentation
   tool; for an example of how to leverage it for other purposes, you can
   have a look at afl-showmap.c.

 */

#include <sys/shm.h>
#include "afl-fast-cov.h"
#include "exec/exec-all.h"

/***************************
 * VARIOUS AUXILIARY STUFF *
 ***************************/

/* This snippet kicks in when the instruction pointer is positioned at
   _start and does the usual forkserver stuff, not very different from
   regular instrumentation injected via afl-as.h. */

#define AFL_QEMU_CPU_SNIPPET2 do { \
    if (itb->pc == afl_entry_point) { \
      afl_setup(); \
      afl_forkserver(cpu); \
    } \
  } while (0)

/* We use one additional file descriptor to relay "needs translation"
   messages between the child and the fork server. */

#define TSL_FD (FORKSRV_FD - 1)

/* This is equivalent to afl-as.h: */

static unsigned char *afl_area_ptr;

/* Exported variables populated by the code patched into elfload.c: */

abi_ulong afl_entry_point, /* ELF entry point (_start) */
          afl_start_code,  /* .text start pointer      */
          afl_end_code;    /* .text end pointer        */

__thread abi_ulong afl_prev_loc;

/* Set in the child process in forkserver mode: */

unsigned char afl_fork_child;
unsigned int afl_forksrv_pid;
unsigned char debug_mode;
unsigned char rq_trace_log_per_run;

/* Instrumentation ratio: */

static unsigned int afl_inst_rms = MAP_SIZE;

/* Function declarations. */

static void afl_setup(void);
static void afl_wait_tsl(CPUState*, int);

/* Data structure passed around by the translate handlers: */

struct afl_tb {
  target_ulong pc;
  target_ulong cs_base;
  uint32_t flags;
  uint32_t cf_mask;
  uint16_t size;
};

struct afl_tsl {
  struct afl_tb tb;
  struct afl_tb last_tb;
  int           tb_exit;
  char          is_chained;
};

/* Some forward decls: */

static inline void tb_add_jump(TranslationBlock *, int, TranslationBlock*);
int open_self_maps(void *cpu_env, int fd);


/* Check if an address is valid in the current mapping */

static inline int is_valid_addr(target_ulong addr) {

  int          flags;
  target_ulong page;

  page = addr & TARGET_PAGE_MASK;

  flags = page_get_flags(page);
  if (!(flags & PAGE_VALID) || !(flags & PAGE_READ)) return 0;

  return 1;

}

/*************************
 * ACTUAL IMPLEMENTATION *
 *************************/

/* Set up SHM region and initialize other stuff. */

static void afl_setup(void) {

  debug_mode = (getenv("AFL_DEBUG") != NULL);

  rq_trace_log_per_run = (getenv("AFL_LOG_PER_THREAD") != NULL);

  char *id_str = getenv(SHM_ENV_VAR),
       *inst_r = getenv("AFL_INST_RATIO");

  if (inst_r) {

    unsigned int r;

    r = atoi(inst_r);

    if (r > 100) r = 100;
    if (!r) r = 1;

    afl_inst_rms = MAP_SIZE * r / 100;

  }

  if (id_str) {

    int shm_id = atoi(id_str);
    afl_area_ptr = shmat(shm_id, NULL, 0);

    if (afl_area_ptr == (void*)-1) exit(1);

    /* With AFL_INST_RATIO set to a low value, we want to touch the bitmap
       so that the parent doesn't give up on us. */

    if (inst_r) afl_area_ptr[0] = 1;

  }

  if (getenv("AFL_INST_LIBS")) {

    afl_start_code = 0;
    afl_end_code   = (abi_ulong)-1;

  }

  if (getenv("AFL_CODE_START"))
    afl_start_code = strtoll(getenv("AFL_CODE_START"), NULL, 16);
  if (getenv("AFL_CODE_END"))
    afl_end_code = strtoll(getenv("AFL_CODE_END"), NULL, 16);

  /* pthread_atfork() seems somewhat broken in util/rcu.c, and I'm
     not entirely sure what is the cause. This disables that
     behaviour, and seems to work alright? */

  rcu_disable_atfork();

  rq_trace_bb(afl_entry_point, 0, afl_prev_loc);

}


/* Fork server logic, invoked once we hit _start. */

void afl_forkserver(CPUState *cpu) {

  static unsigned char tmp[4];

  if (getenv("AFL_QEMU_DEBUG_MAPS")) open_self_maps(cpu->env_ptr, 2);

  /* Tell the parent that we're alive. If the parent doesn't want
     to talk, assume that we're not running in forkserver mode. */

  if (write(FORKSRV_FD + 1, tmp, 4) != 4) return;

  fprintf(stderr, "-- Program output begins --\n");


  afl_forksrv_pid = getpid();

  /* All right, let's await orders... */

  while (1) {

    pid_t child_pid;
    int status, t_fd[2];

    /* Whoops, parent dead? */

    if (read(FORKSRV_FD, tmp, 4) != 4) exit(2);

    /* Establish a channel with child to grab translation commands. We'll
       read from t_fd[0], child will write to TSL_FD. */

    if (pipe(t_fd) || dup2(t_fd[1], TSL_FD) < 0) exit(3);
    close(t_fd[1]);

    child_pid = fork();
    if (child_pid < 0) exit(4);

    if (!child_pid) {

      /* Child process. Close descriptors and run free. */

      afl_fork_child = 1;
      close(FORKSRV_FD);
      close(FORKSRV_FD + 1);
      close(t_fd[0]);
      return;

    }

    /* Parent. */

    close(TSL_FD);

    if (write(FORKSRV_FD + 1, &child_pid, 4) != 4) exit(5);

    /* Collect translation requests until child dies and closes the pipe. */

    afl_wait_tsl(cpu, t_fd[0]);

    /* Get and relay exit status to parent. */

    if (waitpid(child_pid, &status, 0) < 0) exit(6);

    if (write(FORKSRV_FD + 1, &status, 4) != 4) exit(7);

    if(rq_trace_log_per_run) rq_trace_free();

  }

}


/* This code is invoked whenever QEMU decides that it doesn't have a
   translation of a particular block and needs to compute it, or when QEMU
   decides to chain to TBs together. When this happens, we tell the parent 
   to mirror the operation, so that the next fork() has a cached copy. */

void afl_request_tsl(TranslationBlock *tb, uint32_t cf_mask,
                     TranslationBlock *last_tb, int tb_exit) {

  struct afl_tsl t;

  if (!afl_fork_child) return;

  t.tb.pc      = tb->pc;
  t.tb.cs_base = tb->cs_base;
  t.tb.flags   = tb->flags;
  t.tb.cf_mask = cf_mask;
  t.tb.size    = tb->size;
  t.tb_exit    = tb_exit;

  /* if last_tb is not NULL, then it was chained */
  if (last_tb) {

    t.last_tb.pc      = last_tb->pc;
    t.last_tb.cs_base = last_tb->cs_base;
    t.last_tb.flags   = last_tb->flags;
    t.last_tb.cf_mask = cf_mask;
    t.last_tb.size    = last_tb->size;
    t.is_chained      = 1;

  } else {
    t.last_tb.pc = afl_prev_loc;
    t.is_chained = 0;
  }

  if (write(TSL_FD, &t, sizeof(struct afl_tsl)) != sizeof(struct afl_tsl))
    return;

}

/* This is the other side of the same channel. Since timeouts are handled by
   afl-fuzz simply killing the child, we can just wait until the pipe breaks. */

static void afl_wait_tsl(CPUState *cpu, int fd) {

  struct afl_tsl t;
  TranslationBlock *tb, *last_tb;

  while (1) {

    u8 invalid_pc = 0;

    /* Broken pipe means it's time to return to the fork server routine. */

    if (read(fd, &t, sizeof(struct afl_tsl)) != sizeof(struct afl_tsl))
      break;

    if (!rq_trace_log_per_run) rq_trace_bb(t.tb.pc, t.tb.size, t.last_tb.pc);

    tb = tb_htable_lookup(cpu, t.tb.pc, t.tb.cs_base, t.tb.flags, t.tb.cf_mask);

    if(!tb) {

      if (is_valid_addr(t.tb.pc)) {
        mmap_lock();
        tb = tb_gen_code(cpu, t.tb.pc, t.tb.cs_base, t.tb.flags, t.tb.cf_mask);
        mmap_unlock();
      } else {
        invalid_pc = 1;
      }
    }

    if (t.is_chained) {

      if (!invalid_pc) {

        last_tb = tb_htable_lookup(cpu, t.last_tb.pc, t.last_tb.cs_base, t.last_tb.flags, t.last_tb.cf_mask);
        if (last_tb) tb_add_jump(last_tb, t.tb_exit, tb);

      }

    }

  }

  close(fd);

}
