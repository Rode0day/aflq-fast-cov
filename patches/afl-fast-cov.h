

#ifndef __AFL_QEMU_COMMON
#define __AFL_QEMU_COMMON

#include <stdint.h>
#include <stdlib.h>

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;

extern abi_ulong      afl_entry_point, afl_start_code, afl_end_code;
extern __thread abi_ulong afl_prev_loc;
extern unsigned int afl_forksrv_pid;
extern unsigned char afl_fork_child;
extern unsigned char rq_trace_log_per_run;
extern unsigned char debug_mode;

/* Function declarations. */

void afl_forkserver(CPUState*);
void afl_request_tsl(TranslationBlock*, uint32_t, TranslationBlock*, int);

void rq_trace_init(void);
void rq_trace_free(void);
bool rq_trace_bb(abi_ulong, uint16_t, abi_ulong);

/* Designated file descriptors for forkserver commands (the application will
   use FORKSRV_FD and FORKSRV_FD + 1): */

#define FORKSRV_FD          198

/* Maximum line length passed from GCC to 'as' and used for parsing
   configuration files: */

#define MAX_LINE            8192

/* Environment variable used to pass SHM ID to the called program. */

#define SHM_ENV_VAR         "__AFL_SHM_ID"

/* Map size for the traced binary (2^MAP_SIZE_POW2). Must be greater than
   2; you probably want to keep it under 18 or so for performance reasons
   (adjusting AFL_INST_RATIO when compiling is probably a better way to solve
   problems with complex programs). You need to recompile the target binary
   after changing this - otherwise, SEGVs may ensue. */

#define MAP_SIZE_POW2       16
#define MAP_SIZE            (1 << MAP_SIZE_POW2)

#endif
