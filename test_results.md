

## sqlite3 corpus tests

- 16,009 inputs [corpus](https://www.googleapis.com/download/storage/v1/b/fuzzbench-data/o/2020-05-11%2Fexperiment-folders%2Fsqlite3_ossfuzz-afl%2Ftrial-129001%2Fcorpus%2Fcorpus-archive-0097.tar.gz?generation=1589335061074778&alt=media)
- un-instrumented binary [ossfuzz](https://gitlab.com/fuzzing/fuzzbench/-/jobs/571865188/artifacts/raw/benchmarks/sqlite3/noins/ossfuzz)
- libfuzzer binary [sqlite3_ossfuzz.tar.gz](https://www.googleapis.com/download/storage/v1/b/fuzzbench-data/o/2020-05-11%2Fcoverage-binaries%2Fcoverage-build-sqlite3_ossfuzz.tar.gz?generation=1589242154111696&alt=media)

| version | docker os | PCs    | Blocks | Edges  | time          | CI job |
| ------- | --------- | ------ | ------ |------- | ------------- | ------ |
| v2.10.0 | xenial  | 17,326 |        | 56,036 | 2196s (36.6m) | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/577753602) |
| v2.10.0 | bionic  | 17,329 |        | 56,031 | 2188s (36.5m) | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/577753603) |
| v2.10.0 | xenial  | 17,331 |        | 56,031 | 2282s (38.0m) | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/577828899) |
| v2.10.0 | bionic  | 17,327 |        | 56,032 | 2153s (35.8m) | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/577828903) |
|         |         |        |        |        |               |                                                                    |
| v2.12.1 | xenial  | 17,326 |        | 56,026 | 2405s (40.1m) | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/577883653) |
| v2.12.1 | bionic  | 17,326 |        | 56,032 | 2195s (36.6m) | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/577883654) |
| v2.12.1 | xenial  | 17,327 |        | 56,026 | 2380s (39.7m) | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/577883653) |
| v2.12.1 | bionic  | 17,329 |        | 56,038 | 2338s (40.0m) | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/577883654) |
|         |         |        |        |        |               |                                                                    |
| v2.12.1 | xenial  | 17,329 | 38,074 | 56,046 | 195s  (3.3m)  | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/579811530) |
| v2.12.1 | xenial  |        | 38,067 | 56,027 | 2022s (33.7m) | (same) |
| v2.12.1 | bionic  | 17,330 | 38,071 | 56,037 | 170s  (2.8m)  | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/579811531) |
| v2.12.1 | bionic  |        | 38,076 | 56,042 | 1913s (31.9m) | (same) |
|         |         |        |        |        |               |                                                                    |
| v2.12.1 | xenial  | 17,327 | 38,067 | 56,027 | 182s  (3.0m)  | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/580197186) |
| v2.12.1 | xenial  |        | 38,071 | 56,032 | 2005s (33.4m) | (same) |
| v2.12.1 | bionic  | 17,332 | 38,058 | 56,014 | 170s  (2.8m)  | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/580197189) |
| v2.12.1 | bionic  |        | 38,078 | 56,049 | 1955s (32.6m) | (same) |
|         |         |        |        |        |               |                                                                    |
| v3.1.1  | xenial  | 17,327 | 38,069 | 56,031 | 184s (3.1m)   | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/581187551) |
| v3.1.1  | xenial  |        | 38,067 | 56,025 | 2090s (34.8m) | (same) |
| v3.1.1  | bionic  | 17,330 | 38,071 | 56,029 | 174s (2.9m)   | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/576189965) |
| v3.1.1  | bionic  |        | 38,068 | 56,028 | 1960s (32.7m) | (same) |
| v3.1.1  | focal   | 17,331 | 38,065 | 56,027 | 189s (3.2m)   | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/576189966) |
| v3.1.1  | focal   |        | 38,071 | 56,040 | 2032s (33.8m) | (same) |
|         |         |        |        |        |               |                                                                    |
| v3.1.1  | xenial  | 17,324 | 38,070 | 56,032 | 177s (3.0m)   | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/581742359) |
| v3.1.1  | xenial  |        | 38,068 | 56,028 | 2039s (34.0m) | (same) |
| v3.1.1  | bionic  | 17,326 | 38,065 | 56,023 | 175s (2.9m)   | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/581742362) |
| v3.1.1  | bionic  |        | 38,070 | 56,030 | 1959s (32.7m) | (same) |
| v3.1.1  | focal   | 17,328 | 38,063 | 56,016 | 200s (3.3m)   | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/581742369) |
| v3.1.1  | focal   |        | 38,063 | 56,023 | 2048s (34.1m) | (same) |
|         |         |        |        |        |               |                                                                    |
| v5.0.0  | xenial  | 17,326 | 38,066 | 56,021 | 192s (3.2m)   | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/582082095) |
| v5.0.0  | xenial  |        | 38,069 | 56,030 | 2133s (35.6m) | (same) |
| v5.0.0  | bionic  | 17,325 | 38,068 | 56,031 | 174s (2.9m)   | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/582082096) |
| v5.0.0  | bionic  |        | 38,055 | 56,012 | 1978s (33.0m) | (same) |
| v5.0.0  | focal   | 17,281 | 38,083 | 56,047 | 200s (3.3m)   | [job](https://gitlab.com/wideglide/aflq-fast-cov/-/jobs/582082097) |
| v5.0.0  | focal   |        | 38,067 | 56,024 | 2136s (35.6m) | (same) |
