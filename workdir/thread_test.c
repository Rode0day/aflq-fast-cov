#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>

#ifdef SYS_gettid
#define gettid() syscall(SYS_gettid)
#endif


pthread_t tid[2];

void* doSomeThing(void *arg)
{
    unsigned long i = 0;
    pthread_t id = pthread_self();
    pid_t t_id = gettid();

    if(pthread_equal(id,tid[0]))
    {
        printf("[*] First thread processing %u\n", t_id);
    }
    else
    {
        printf("[*] Second thread processing %u\n",  t_id);
    }

    for(i=0; i<(0xFFFFFF);i++);

    return NULL;
}

void create_thread(int i) {
    int err = pthread_create(&(tid[i]), NULL, &doSomeThing, NULL);
    if (err != 0)
        printf("[-] Error: can't create thread :[%s]", strerror(err));
    else
        printf("   Thread created successfully\n");
}

void join_threads(int i) {
    while (--i > 0) pthread_join(tid[i], NULL);
}

int main(void)
{
    int i = 0;

    printf("[*] threading coverage test starting PID:%u TID:%lu\n", getpid(), gettid());

    while(i < 2)
    {
        create_thread(i);
        i++;
    }

    join_threads(i);

    return 0;
}
