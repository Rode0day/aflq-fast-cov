;  nasm -f elf64 -o test.o test.asm
;  gcc -o test test.o

default rel

global foo
global test
global main

extern __stack_chk_fail
extern __isoc99_scanf
extern puts
extern printf


SECTION .text

foo:
        push    rbp
        mov     rbp, rsp
        sub     rsp, 16
        mov     esi, edi
        lea     rdi, [rel str_got]
        call    printf
        nop
        leave
        ret


test:
        push    rbp
        mov     rbp, rsp
        sub     rsp, 16
        mov     dword [rbp-4], edi
        mov     dword [rbp-8], esi
        jmp     L_003

L_001:  cmp     dword [rbp-8], 0
        jnz     L_002
        lea     rdi, [rel str_lt]
        call    puts
        jmp     L_005

L_002:  sub     dword [rbp-4], 1
        sub     dword [rbp-8], 1
L_003:  cmp     dword [rbp-4], 0
        jg      L_001
        cmp     dword [rbp-8], 0
        jnz     L_004
        lea     rdi, [rel str_eq]
        call    puts
        jmp     L_005

L_004:  lea     rdi, [rel str_gt]
        call    puts
        nop
L_005:  leave
        ret


main:
        push    rbp
        mov     rbp, rsp
        sub     rsp, 16


        mov     rax, qword [fs:abs 28H]
        mov     qword [rbp-8], rax
        xor     eax, eax
        lea     rdi, [rel str_enter1]
        call    puts
        lea     rax, [rbp-10H]
        mov     rsi, rax
        lea     rdi, [rel str_int]
        mov     eax, 0
        call    __isoc99_scanf
        lea     rdi, [rel str_enter2]
        call    puts
        lea     rax, [rbp-0CH]
        mov     rsi, rax
        lea     rdi, [rel str_int]
        mov     eax, 0
        call    __isoc99_scanf
        mov     eax, dword [rbp-10H]
        test    eax, eax
        jle     L_006
        mov     eax, dword [rbp-10H]
        cmp     eax, 49
        jg      L_006
        mov     eax, dword [rbp-0CH]
        test    eax, eax
        jle     L_006
        mov     eax, dword [rbp-0CH]
        cmp     eax, 49
        jg      L_006
        mov     eax, dword [rbp-10H]
        mov     edi, eax
        call    foo
        mov     eax, dword [rbp-0CH]
        mov     edi, eax
        call    foo
        mov     edx, dword [rbp-0CH]
        mov     eax, dword [rbp-10H]
        mov     esi, edx
        mov     edi, eax
        call    test
L_006:  mov     eax, 0
        mov     rcx, qword [rbp-8]


        xor     rcx, qword [fs:abs 28H]
        jz      L_007
        call    __stack_chk_fail
L_007:  leave
        ret



SECTION .data


SECTION .bss


SECTION .rodata

str_enter1: db 'Enter somthing: ', 0
str_enter2: db 'Enter something else: ', 0
str_got: db 'got %d',10, 0
str_int: db '%d', 0
str_lt: db 'b<a', 0
str_eq: db 'b==a', 0
str_gt: db 'b>a', 0
